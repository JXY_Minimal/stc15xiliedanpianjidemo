#include <STC15.H>
sbit driver_output = P3^2;
sbit bridge_output = P3^4;
sbit key = P3^5;

void delay_ms(unsigned int z){
	unsigned int x,y;
	for(x = z; x > 0; x--)
		for(y = 120; y > 0 ; y--);
}

void main(){
/*  设置引脚模式  */
  P3M0=	0x14;
  P3M1=	0x00;
/* 将3.2、3.4设为推挽输出*/

  key=0;					 //输入引脚初始值设为低电平
  driver_output=0;		   	 //driver_output 初始化
  bridge_output=0;			 //bridge_output 初始化
  
  delay_ms(2500);
  while(1){
     if(key==1){
     	driver_output=1;
        delay_ms(2500);
     	bridge_output=1;
     }
     if(key==0){
    	bridge_output=0;
        delay_ms(5000);
     	driver_output=0;
     }
  }
}